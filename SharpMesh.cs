﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.DXGI;
using SharpDX.Direct3D11;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;

using Buffer11 = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;
using MapFlags = SharpDX.Direct3D11.MapFlags;
using System.IO;


namespace SharpHelper
{
    public class SharpMesh : IDisposable
    {
        public SharpDevice Device { get;  set; }
        public Buffer11 VertexBuffer { get;  set; }
        public Buffer11 IndexBuffer { get;  set; }
        public int VertexSize { get;  set; }
        public List<SharpSubSet> SubSets { get;  set; }
        public SharpMesh(SharpDevice device)
        {
            Device = device;
            SubSets = new List<SharpSubSet>();
        }
        public void Draw(int subset)
        {
            Device.DeviceContext.DrawIndexed(SubSets[subset].IndexCount, SubSets[subset].StartIndex, 0);
        }
        public void Begin()
        {
            Device.DeviceContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            Device.DeviceContext.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(VertexBuffer, VertexSize, 0));
            Device.DeviceContext.InputAssembler.SetIndexBuffer(IndexBuffer, Format.R32_UInt, 0);
        }
        public void Dispose()
        {
            VertexBuffer.Dispose();
            IndexBuffer.Dispose();
            foreach (var s in SubSets)
            {
                if (s.DiffuseMap != null)
                    s.DiffuseMap.Dispose();

                if (s.NormalMap != null)
                    s.NormalMap.Dispose();
            }
        }
    }
}
