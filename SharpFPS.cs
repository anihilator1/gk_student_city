﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpHelper
{
    public class SharpFPS
    {
        public int FPS { get; set; }
        private int n1;
        private long timeout;
        private System.Diagnostics.Stopwatch watch;
        public SharpFPS()
        {
            watch = new System.Diagnostics.Stopwatch();
            Reset();
        }
        public void Reset()
        {
            watch.Reset();
            n1 = 0;
            timeout = watch.ElapsedMilliseconds;
            FPS = 0;
            watch.Start();
        }
        public void Update()
        {
            n1++;
            if (watch.ElapsedMilliseconds - timeout >= 1000)
            {
                FPS = n1;
                n1 = 0;
                timeout = watch.ElapsedMilliseconds;
            }
        }
    }
}
