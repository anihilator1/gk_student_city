﻿using SharpDX;
using SharpHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GK_Project4
{
    class ModelMenager
    {
        public List<Model> models;
        private List<IMovingModel> movingModels;
        public IEnumerable<Model> Models
        {
            get
            {
                foreach (Model model in models)
                {
                    yield return model;

                }
            }
        }
        public IEnumerable<Model> ObservedModels
        {
            get
            {
                foreach (Model model in Models)
                {
                    if (model.ModelCamera == CameraEnum.ObservedByCamera)
                    {
                        yield return model;
                    }
                }
            }
        }
        public IEnumerable<Model> ModelsWithCamera
        {
            get
            {
                foreach (Model model in Models)
                {
                    if (model.ModelCamera == CameraEnum.WithCamera)
                    {
                        yield return model;
                    }
                }
            }
        }
        public IEnumerator<RoadModel> RoadModels
        {
            get
            {
                foreach (Model model in Models)
                {
                    if (model.GetType() == typeof(RoadModel))
                    {
                        yield return (RoadModel)model;
                    }
                }
            }
        }
        public void Update()
        {
            foreach (IMovingModel movingModel in movingModels)
            {
                movingModel.Update();
            }
        }
        public ModelMenager(SharpDevice sharpDevice)
        {
            models = new List<Model>();
            movingModels = new List<IMovingModel>();

            models.Add(new Model(sharpDevice, "city", 5f, new Vector3(0, 0, 0)));

            AddMovingModel(new CircleMovingModel(sharpDevice, "zepellin", 2f, -0.3535922f, CameraEnum.ObservedByCamera, new Vector3(0, 40, 0), 60f));

            Model smallCar = new Model(sharpDevice, "small_car", 0.003663758f, new Vector3(-7.449315f, 0.2626542f, -42.10251f));
            models.Add(smallCar);
            models.Add(new Model(smallCar, 0.003663758f, new Vector3(-13.50654f, 0.2626542f, -38.8783f)));
            models.Add(new Model(smallCar, 0.003663758f, new Vector3(-10.56245f, 0.2626542f, -35.85477f)));
            models.Add(new Model(smallCar, 0.003663758f, new Vector3(-15.92841f, 0.2626542f, -32.54425f)));
            models.Add(new Model(smallCar, 0.003663758f, new Vector3(-7.786129f, 0.2626542f, -32.75926f)));

            Model car2 = new Model(sharpDevice, "car2", 0.003663758f, new Vector3(-7.449315f, -0.01986704f, -42.10251f));
            models.Add(car2);
            models.Add(new Model(car2, 0.01911294f, new Vector3(-15.86806f, -0.01986704f, -41.92158f), 3.155216f));
            models.Add(new Model(car2, 0.01911294f, new Vector3(-13.82977f, -0.01986704f, -35.94822f), 3.155216f));
            models.Add(new Model(car2, 0.01911294f, new Vector3(-6.144546f, -0.01986704f, -35.94822f), 3.155216f));

            AddMovingModel(new RoadModel(smallCar, 0.003663758f, -1.555235f, 0, CameraEnum.WithCamera, GetRoad1()));
            AddMovingModel(new RoadModel(car2, 0.01911294f, 0.6377122f, 6, CameraEnum.WithCamera, GetRoad1(), -0.01986704f));
            AddMovingModel(new RoadModel(car2, 0.01911294f, 0.9672171f, 13, CameraEnum.WithCamera, GetRoad1(), -0.01986704f));
            AddMovingModel(new RoadModel(smallCar, 0.003663758f, -3.807976f, 18, CameraEnum.WithCamera, GetRoad1()));
            AddMovingModel(new RoadModel(car2, 0.01911294f, 4.355641f, 24, CameraEnum.WithCamera, GetRoad1(), -0.01986704f));
            AddMovingModel(new RoadModel(smallCar, 0.003663758f, 0.2236403f, 30, CameraEnum.WithCamera, GetRoad1()));


            AddMovingModel(new RoadModel(smallCar, 0.003663758f, 1.544765f, 0, CameraEnum.WithCamera, GetRoad2()));
            AddMovingModel(new RoadModel(car2, 0.01911294f, -0.06228782f, 5, CameraEnum.WithCamera, GetRoad2(), -0.01986704f));
            AddMovingModel(new RoadModel(car2, 0.01911294f, 1.267217f, 10, CameraEnum.WithCamera, GetRoad2(), -0.01986704f));
            AddMovingModel(new RoadModel(smallCar, 0.003663758f, 4.167215f, 15, CameraEnum.WithCamera, GetRoad2()));
            AddMovingModel(new RoadModel(car2, 0.01911294f, 9.455642f, 20, CameraEnum.WithCamera, GetRoad2(), -0.01986704f));
            AddMovingModel(new RoadModel(smallCar, 0.003663758f, -0.2763597f, 25, CameraEnum.WithCamera, GetRoad2()));

            Model skybox = new Model(sharpDevice, "cube",1000, new Vector3(0, 0, 0));
            models.Add(skybox);

            Model moon= new Model(sharpDevice, "moon", 0.1f, new Vector3(0, 0, 0));
            models.Add(moon);

        }
        private List<Vector3> GetRoad1()
        {
            return new List<Vector3>()
            {
                new Vector3(-20.37991f,0.2626542f,-45.83603f),
                new Vector3(18.45212f,0.2626542f,-45.83603f),
                new Vector3(25.97939f,0.2626542f,-44.76208f),
                new Vector3(29.13675f,0.2626542f,-43.77084f),
                new Vector3(31.37239f,0.2626542f,-42.41272f),
                new Vector3(33.75123f,0.2626542f,-40.69402f),
                new Vector3(36.56974f,0.2626542f,-38.22395f),
                new Vector3(39.94134f,0.2626542f,-33.06999f),
                new Vector3(42.44292f,0.2626542f,-27.80215f),
                new Vector3(42.44272f,0.2626542f,-14.44604f),
                new Vector3(42.40945f,0.2626542f,-1.669348f),
                new Vector3(41.85698f,0.2626542f,4.296548f),
                new Vector3(41.84482f,0.2626542f,7.978401f),
                new Vector3(43.21468f,0.2626542f,10.20596f),
                new Vector3(70.62569f,0.2626542f,29.89604f),
                new Vector3(77.52297f,0.2626542f,37.72622f),
                new Vector3(79.03071f,0.2626542f,44.57005f),
                new Vector3(78.70723f,0.2626542f,53.28605f),
                new Vector3(74.18457f,0.2626542f,61.69584f),
                new Vector3(65.8009f,0.2626542f,69.89375f),
                new Vector3(57.49632f,0.2626542f,72.99512f),
                new Vector3(48.1338f,0.2626542f,71.73904f),
                new Vector3(41.48299f,0.2626542f,68.75357f),
                new Vector3(18.80016f,0.2626542f,53.46083f),
                new Vector3(6.317776f,0.2626542f,47.71371f),
                new Vector3(-13.14153f,0.2626542f,42.07556f),
                new Vector3(-24.93476f,0.2626542f,39.19556f),
                new Vector3(-39.47972f,0.2626542f,35.97887f),
                new Vector3(-46.53429f,0.2626542f,32.42404f),
                new Vector3(-50.33446f,0.2626542f,25.99331f),
                new Vector3(-52.3532f,0.2626542f,20.75378f),
                new Vector3(-53.47556f,0.2626542f,10.93423f),
                new Vector3(-53.51624f,0.2626542f,-8.241348f),
                new Vector3(-53.53406f,0.2626542f,-25.67562f),
                new Vector3(-51.53098f,0.2626542f,-32.38198f),
                new Vector3(-49.23817f,0.2626542f,-37.01086f),
                new Vector3(-44.1804f,0.2626542f,-44.57721f),
                new Vector3(-41.89542f,0.2626542f,-45.71862f),
            };
        }
        private List<Vector3> GetRoad2()
        {
            return new List<Vector3>()
            {
                new Vector3(17.79009f,0.2626542f,-48.55638f),
                new Vector3(-43.13665f,0.2626542f,-48.90616f),
                new Vector3(-48.04095f,0.2626542f,-43.72336f),
                new Vector3(-50.79755f,0.2626542f,-38.45198f),
                new Vector3(-54.69749f,0.2626542f,-31.25208f),
                new Vector3(-56.29747f,0.2626542f,-23.85205f),
                new Vector3(-56.29747f,0.2626542f,15.14803f),
                new Vector3(-54.89749f,0.2626542f,24.14806f),
                new Vector3(-52.99752f,0.2626542f,29.24808f),
                new Vector3(-48.29759f,0.2626542f,34.84805f),
                new Vector3(-42.49768f,0.2626542f,38.44799f),
                new Vector3(-36.99776f,0.2626542f,40.24797f),
                new Vector3(-26.59782f,0.2626542f,42.04794f),
                new Vector3(-0.8977532f,0.2626542f,48.64784f),
                new Vector3(12.80226f,0.2626542f,53.34777f),
                new Vector3(39.40222f,0.2626542f,70.74751f),
                new Vector3(48.70208f,0.2626542f,75.74743f),
                new Vector3(55.90197f,0.2626542f,76.74741f),
                new Vector3(66.00182f,0.2626542f,73.64746f),
                new Vector3(74.50169f,0.2626542f,66.34757f),
                new Vector3(82.50156f,0.2626542f,53.64777f),
                new Vector3(82.50156f,0.2626542f,42.04794f),
                new Vector3(78.10163f,0.2626542f,33.14808f),
                new Vector3(70.80174f,0.2626542f,25.94807f),
                new Vector3(46.90211f,0.2626542f,9.54801f),
                new Vector3(43.80215f,0.2626542f,5.348007f),
                new Vector3(45.60213f,0.2626542f,-0.5519904f),
                new Vector3(45.50213f,0.2626542f,-21.95204f),
                new Vector3(44.30215f,0.2626542f,-32.45207f),
                new Vector3(37.50225f,0.2626542f,-41.95192f),
                new Vector3(29.60233f,0.2626542f,-46.85185f),
            };
        }
        private void AddMovingModel<T>(T movingModel) where T : Model, IMovingModel
        {
            models.Add(movingModel);
            movingModels.Add(movingModel);
        }

        public void Dispose()
        {
            foreach (Model model in models)
            {
                model.SharpMesh.Dispose();
            }
        }
    }
}
