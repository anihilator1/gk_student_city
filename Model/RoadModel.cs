﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpHelper;

namespace GK_Project4
{
    class RoadModel : Model, IMovingModel
    {
        private List<Vector3> road;
        private int current;
        private float oldAngle;
        public Vector4 Direction
        {
            get
            {
                int next = (current + 1) % road.Count;
                Vector3 direction = road[next] - this.Position;
                direction.Normalize();
                return new Vector4(direction.X, -0.2f, direction.Z, 0);
            }
        }
        public RoadModel(Model model, float scale, float rotation, int current, CameraEnum modelCamera, List<Vector3> road,float elevation= 0.2626542f) : base(model, scale, new Vector3(road[current].X,elevation,road[current].Z), rotation, modelCamera)
        {
            this.road = road;
            this.current = current;
            Vector3 tmp = road[(current + 1) % road.Count] - road[current];
            oldAngle = (float)Math.Atan(tmp.Z / tmp.X);
            for(int i=0;i<road.Count;i++)
            {
                road[i] = new Vector3(road[i].X, elevation, road[i].Z);
            }
        }


        public RoadModel(SharpDevice sharpDevice, string name, float scale, float rotation, int current, CameraEnum modelCamera, List<Vector3> road, float elevation = 0.2626542f) : base(sharpDevice, name, scale, road[current], rotation, modelCamera)
        {
            this.road = road;
            this.current = current;
            Vector3 tmp = road[(current + 1) % road.Count] - road[current];
            oldAngle = (float)Math.Atan(tmp.Z / tmp.X);
            for (int i = 0; i < road.Count; i++)
            {
                road[i] = new Vector3(road[i].X, elevation, road[i].Z);
            }

        }
        public void Update()
        {
            int next = (current + 1) % road.Count;
            Vector3 direction = road[next] - this.Position;

            if (direction.Length() < 1f / 10f)
            {
                current = next;
                next = (current + 1) % road.Count;
                Vector3 tmp = road[next] - road[current];
                float newAngle = (float)Math.Atan(tmp.Z / tmp.X);
                float rotation = newAngle - oldAngle;
                while (rotation < -Math.PI / 2)
                {
                    rotation += (float)Math.PI;
                }
                while (rotation > Math.PI / 2) rotation -= (float)Math.PI;
                this.Rotation -= rotation;
                oldAngle = newAngle;
                this.Position += direction;
                this.Target += direction;
            }
            else
            {
                direction.Normalize();
                this.Position += direction / 10;
                this.Target += direction / 10;

            }
        }
    }
}
