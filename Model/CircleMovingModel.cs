﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpHelper;

namespace GK_Project4
{
    class CircleMovingModel : Model, IMovingModel
    {
        private Vector3 center;
        private float radius;
        public CircleMovingModel(SharpDevice sharpDevice, string name, float scale, float rotation, CameraEnum modelCamera,Vector3 center,float radius) : base(sharpDevice, name, scale, new Vector3(center.X,center.Y,center.Z+radius), rotation, modelCamera)
        {
            this.center = center;
            this.radius = radius;
        }
        public void Update()
        {
            Vector3 tmp = this.Position - center;
            Matrix rotationMatrix = Matrix.RotationY(0.001f);
            tmp = Vector3.TransformCoordinate(tmp, rotationMatrix);
            this.Position = tmp + center;
            this.Rotation += 0.001f;
        }
    }
}
