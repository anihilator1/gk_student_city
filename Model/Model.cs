﻿using SharpDX;
using SharpHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GK_Project4
{
    class Model
    {
        public Matrix World
        {
            get
            {
                return Matrix.RotationY(Rotation) * Matrix.Scaling(Scale) * Matrix.Translation(Position);
            }
        }
        public SharpMesh SharpMesh { get; private set; }
        public float Scale;
        public Vector3 Position;
        public virtual Vector3 Target { get;  set; }
        public float Rotation;
        public CameraEnum ModelCamera { get; private set; }
        public SharpDevice SharpDevice { get; private set; }
        private bool flag = false;
        public Model(SharpDevice sharpDevice, string name, float scale, Vector3 position, float rotation = 0, CameraEnum modelCamera = CameraEnum.None)
        {
            this.SharpDevice = sharpDevice;
            Scale = scale;
            Position = position;
            Rotation = rotation;
            this.ModelCamera = modelCamera;
            SharpMesh = LoadModel(sharpDevice, name);
            Target = new Vector3();
        }
        public Model(Model model, float scale, Vector3 position, float rotation = 0, CameraEnum modelCamera = CameraEnum.None)
        {
            this.SharpDevice = model.SharpDevice;
            Scale = scale;
            Position = position;
            Rotation = rotation;
            this.ModelCamera = modelCamera;
            SharpMesh = model.SharpMesh;
        }
        private SharpMesh LoadModel(SharpDevice sharpDevice, string name)
        {
            string path = "../../Models/" + name + "/" + name + ".obj";
            return ObjImporter.GetMeshFromFile(sharpDevice, path);
        }
        public void DrawModel()
        {
            SharpMesh.Begin();
            for (int i = 0; i <SharpMesh.SubSets.Count; i++)
            {
                SharpDevice.DeviceContext.PixelShader.SetShaderResource(0, SharpMesh.SubSets[i].DiffuseMap);
                SharpMesh.Draw(i);
            }
        }
        public void UpdatePosition(object sender, KeyEventArgs keyEventArgs)
       {

            switch (keyEventArgs.KeyCode)
            {
                case Keys.Up:
                    Position.X += 0.1f;
                    flag = false;
                    break;
                case Keys.Down:
                    Position.X -= 0.1f;
                    flag = false;
                    break;
                case Keys.Left:
                    Position.Z += 0.1f;
                    break;
                case Keys.Right:
                    Position.Z -= 0.1f;
                    flag = false;
                    break;
                case Keys.Enter:
                    Position.Y += 0.01f;
                    flag = false;
                    break;
                case Keys.ShiftKey:
                    Position.Y -= 0.01f;
                    flag = false;
                    break;
                case Keys.PageUp:
                    Scale += 0.0001f;
                    flag = false;
                    break;
                case Keys.PageDown:
                    Scale -= 0.0001f;
                    flag = false;
                    break;
                case Keys.Q:
                    Rotation -= 0.1f;
                    flag = false;
                    break;
                case Keys.E:
                    Rotation += 0.1f;
                    flag = false;
                    break;
                case Keys.I:
                    if (!flag)
                    {
                        Console.WriteLine("Cords:");
                        Console.WriteLine("Scale:" + Scale+"f");
                        Console.WriteLine("Rotation:" + Rotation+"f");
                        Console.WriteLine("Translation:");
                        Console.WriteLine("X:" + Position.X+"f");
                        Console.WriteLine("Y:" + Position.Y+"f");
                        Console.WriteLine("Z:" + Position.Z+"f");

                        flag = true;
                    }
                    break;

            }
        }
    }
}
