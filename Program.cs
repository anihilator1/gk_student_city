﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GK_Project4;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Windows;
using SharpHelper;
using SharpDxBuffer = SharpDX.Direct3D11.Buffer;

namespace GK_Project4
{
    class Program
    {
        static Camera camera;
        static CameraMenager cameraMenager;
        private static void KeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            switch (keyEventArgs.KeyCode)
            {
                case Keys.V:
                    camera = cameraMenager.NextCamera();
                    break;
            }
        }
        private static RenderForm InitForm(CameraMenager cameraMenager)
        {
            RenderForm renderForm = new RenderForm();
            renderForm.Text = "Student City";
            renderForm.KeyDown += cameraMenager.FlyingCamera.UpdatePosition;
            renderForm.KeyDown += KeyDown;
            renderForm.MouseMove += cameraMenager.FlyingCamera.UpdateAngle;
            return renderForm;
        }
        private static void InitModelCameras(RenderForm renderForm, CameraMenager cameraMenager, ModelMenager modelMenager)
        {
            cameraMenager.CreateModelCameras(modelMenager.ModelsWithCamera);
            foreach (ModelCamera modelCamera in cameraMenager.ModelCameras)
            {
                renderForm.MouseMove += modelCamera.UpdateAngle;
            }
        }
        private static Matrix GetProjectionMatrix(RenderForm renderForm)
        {
            float ratio = (float)renderForm.ClientRectangle.Width / (float)renderForm.ClientRectangle.Height;
            Matrix projection = Matrix.PerspectiveFovLH(3.14F / 3.0F, ratio, 1F, 1000.0F);
            return projection;
        }
        static void Main(string[] args)
        {
            if (!SharpDevice.IsDirectX11Supported())
            {
                System.Windows.Forms.MessageBox.Show("DirectX11 Not Supported");
                return;
            }

            cameraMenager = new CameraMenager();
            RenderForm renderForm = InitForm(cameraMenager);
            camera = cameraMenager.NextCamera();
            SharpFPS sharpFPS = new SharpFPS();
            using (SharpDevice sharpDevice = new SharpDevice(renderForm))
            {
                ModelMenager modelMenager = new ModelMenager(sharpDevice);
                cameraMenager.CreateObservingCameras(modelMenager.ObservedModels);
                ShaderMenager shaderMenager = new ShaderMenager(sharpDevice);

                shaderMenager.AddEffectsControl(renderForm);

                InitModelCameras(renderForm, cameraMenager, modelMenager);
                sharpFPS.Reset();
                renderForm.KeyDown += modelMenager.models[modelMenager.models.Count - 1].UpdatePosition;
                FPSLimiter fPSLimiter = new FPSLimiter(60);
                RenderLoop.Run(renderForm, () =>
                {
                    if (sharpDevice.MustResize)
                    {
                        sharpDevice.Resize();
                    }
                    sharpDevice.UpdateAllStates();
                    sharpDevice.Clear(new Color(0, 0, 0));
                    shaderMenager.ApplyShader();
                    modelMenager.Update();
                    cameraMenager.Update();
                    foreach (Model model in modelMenager.Models)
                    {
                        Matrix projection = GetProjectionMatrix(renderForm);
                        Matrix view = camera.View;
                        Matrix world = model.World;

                        shaderMenager.Update(world, projection, camera, modelMenager.RoadModels,cameraMenager.FlyingCamera);
                        model.DrawModel();
                    }
                    DrawFps(sharpDevice, sharpFPS);
                    sharpDevice.Present();
                    fPSLimiter.LimitFPS();
                    cameraMenager.FlyingCamera.Enabled = true;
                });
                modelMenager.Dispose();
                shaderMenager.Dispose();
            }


        }
        static void DrawFps(SharpDevice sharpDevice, SharpFPS sharpFPS)
        {
            sharpDevice.Font.Begin();
            sharpFPS.Update();
            sharpDevice.Font.DrawString("FPS: " + sharpFPS.FPS, 0, 0);
            sharpDevice.Font.End();
        }
        static SharpMesh LoadModel(SharpDevice sharpDevice)
        {
            SharpMesh sharpMesh = ObjImporter.GetMeshFromFile(sharpDevice, "../../Models/city/city.obj");
            return sharpMesh;
        }
        static SharpShader InitShader(SharpDevice sharpDevice)
        {
            SharpShader sharpShader = new SharpShader(sharpDevice, "../../Shader/HLSL.hlsl",
                  new SharpShaderDescription() { VertexShaderFunction = "VS", PixelShaderFunction = "PS" },
                  new InputElement[] {
                        new InputElement("POSITION", 0, Format.R32G32B32_Float, 0, 0),
                        new InputElement("NORMAL", 0, Format.R32G32B32_Float, 12, 0),
                        new InputElement("TEXCOORD", 0, Format.R32G32_Float, 24, 0),
                  });
            return sharpShader;
        }
    }
}
