﻿using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Assimp;
using HelixToolkit.Wpf.SharpDX.Model;
using SharpDX.Direct3D11;
using SharpHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDxBuffer = SharpDX.Direct3D11.Buffer;

namespace SharpHelper
{
    public class ObjImporter
    {
        public static SharpMesh GetMeshFromFile(SharpDevice device, string path)
        {
            ObjReader objReader = new ObjReader();
            List<Object3D> object3DList = objReader.Read(path);
            List<WaveFrontModel> waveFrontModelList = ConvertObject3DListToWaveFrontModel(object3DList);
            string[] paths = GetTexturePaths(path);
            return FromObjFile(device, waveFrontModelList.ToArray(), path, paths);
        }
        private static List<WaveFrontModel> ConvertObject3DListToWaveFrontModel(List<Object3D> list)
        {
            List<WaveFrontModel> resultList = new List<WaveFrontModel>();
            foreach (Object3D object3D in list)
            {
                resultList.Add(WaveFrontModel.FromObject3D(object3D));
            }
            return resultList;
        }
        private static SharpMesh FromObjFile(SharpDevice sharpDevice, WaveFrontModel[] waveFrontModelTab, string fileName, string[] texturePaths)
        {
            SharpMesh mesh = new SharpMesh(sharpDevice);
            List<StaticVertex> vertices = new List<StaticVertex>();
            List<int> indices = new List<int>();

            int vcount = 0;
            int icount = 0;
            for (int id = 0; id < waveFrontModelTab.Length; id++)
            {
                WaveFrontModel model = waveFrontModelTab[id];
                vertices.AddRange(model.VertexData);
                indices.AddRange(model.IndexData.Select(i => i + vcount));

                Material material = model.MeshMaterial.First();

                ShaderResourceView tex = null;
                if (texturePaths.Length > id)
                {
                    string textureFile = Path.GetDirectoryName(fileName) + "\\" + Path.GetFileName(texturePaths[id]);
                    tex = sharpDevice.LoadTextureFromFile(textureFile);
                }

                mesh.SubSets.Add(new SharpSubSet()
                {
                    IndexCount = model.IndexData.Count,
                    StartIndex = icount,
                    DiffuseMap = tex
                });

                vcount += model.VertexData.Count;
                icount += model.IndexData.Count;
            }

            mesh.VertexBuffer = SharpDxBuffer.Create<StaticVertex>(sharpDevice.Device, BindFlags.VertexBuffer, vertices.ToArray());
            mesh.IndexBuffer = SharpDxBuffer.Create(sharpDevice.Device, BindFlags.IndexBuffer, indices.ToArray());
            mesh.VertexSize = SharpDX.Utilities.SizeOf<StaticVertex>();

            return mesh;
        }
        private static List<string> ReadLines(string fileName)
        {
            List<string> lines = new List<string>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string l = reader.ReadLine();
                    if (!string.IsNullOrEmpty(l.Trim()) && !l.Contains("#")) lines.Add(l);
                }
            }
            return lines;
        }
        private static List<NamePath> GetMaterials(List<string> lines, string directoryPath)
        {
            List<NamePath> materials = new List<NamePath>();
            foreach (string line in lines)
            {
                if (line.Contains("mtllib "))
                {
                    string path = directoryPath + "\\" + line.Replace("mtllib", "").Trim();
                    materials.AddRange(GetMaterialTexturePaths(path));
                }
            }
            return materials;
        }
        private static string[] GetPaths(List<string> lines, List<NamePath> materials)
        {
            List<string> paths = new List<string>();
            foreach (string line in lines)
            {
                if (line.Contains("usemtl "))
                {
                    string path = (from m in materials where m.name == line.Replace("usemtl", "").Trim() select m.path).FirstOrDefault();
                    if (path != null) paths.Add(path);
                }
            }
            return paths.ToArray();
        }
        private static string[] GetTexturePaths(string fileName)
        {
            List<string> lines = ReadLines(fileName);
            List<NamePath> materials = GetMaterials(lines, System.IO.Path.GetDirectoryName(fileName));
            return GetPaths(lines, materials);
        }
        private static List<NamePath> GetMaterialTexturePaths(string fileName)
        {
            List<NamePath> materials = new List<NamePath>();
            NamePath current = null;
            List<string> lines = ReadLines(fileName);
            foreach (string line in lines)
            {
                if (line.Contains("newmtl"))
                {
                    if (current != null) materials.Add(current);
                    current = new NamePath();
                    current.name = line.Replace("newmtl", "").Trim();
                }
                else if (line.Contains("map_Kd"))
                {
                    current.path = line.Replace("map_Kd", "").Trim();
                }
            }
            if (current != null) materials.Add(current);
            return materials;
        }
    }
}
