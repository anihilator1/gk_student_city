﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;

namespace SharpHelper
{
    public class SharpSubSet
    {
        public ShaderResourceView DiffuseMap { get; set; }
        public ShaderResourceView NormalMap { get; set; }
        public Vector4 AmbientColor { get; set; }
        public Vector4 DiffuseColor { get; set; }
        public Vector4 SpecularColor { get; set; }
        public int SpecularPower { get; set; }
        public Vector4 EmissiveColor { get; set; }
        public int StartIndex { get; set; }
        public int IndexCount { get; set; }
    }
}
