﻿using HelixToolkit.Wpf.SharpDX.Model;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpHelper
{
    public class Material
    {
        public string MaterialName { get; private set; }
        public Vector3 Ambient { get; private set; }
        public Vector3 Diffuse { get; private set; }
        public Vector3 Specular { get; private set; }
        public float Shininess { get; private set; }
        public string DiffuseMap { get; private set; }
        public string NormalMap { get; private set; }
        public Material(Vector3 ambient,Vector3 diffuse,Vector3 specular)
        {
            Ambient = ambient;
            Diffuse = diffuse;
            Specular = specular;
        }
        public static Material FromMaterialCore(MaterialCore materialCore)
        {
            PhongMaterialCore phongMaterialCore = materialCore as PhongMaterialCore;
            Vector3 ambient = new Vector3(phongMaterialCore.AmbientColor.Red, phongMaterialCore.AmbientColor.Green, phongMaterialCore.AmbientColor.Blue);
            Vector3 diffuse = new Vector3(phongMaterialCore.DiffuseColor.Red, phongMaterialCore.DiffuseColor.Green, phongMaterialCore.DiffuseColor.Blue);
            Vector3 specular = new Vector3(phongMaterialCore.SpecularColor.Red, phongMaterialCore.SpecularColor.Green, phongMaterialCore.SpecularColor.Blue);
            Material material = new Material(ambient, diffuse, specular);
            return material;
        }
    }
}
