﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SharpDX;
using System.Globalization;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Model;

namespace SharpHelper
{
    public class WaveFrontModel
    {
        private static CultureInfo infos = CultureInfo.InvariantCulture;
        public string Name { get; set; }
        public List<StaticVertex> VertexData { get; set; }
        public List<TangentVertex> TangentData { get; set; }
        public List<int> IndexData { get; set; }
        public List<Material> MeshMaterial { get; set; }
        public List<int> FaceCounts { get; set; }
        public WaveFrontModel()
        {
            this.IndexData = new List<int>();
            this.Name = "";
            this.VertexData = new List<StaticVertex>();
            this.MeshMaterial = new List<Material>();
            this.FaceCounts = new List<int>();
        }
        public static WaveFrontModel FromObject3D(Object3D object3D)
        {
            MeshGeometry3D meshGeometry3D = object3D.Geometry as MeshGeometry3D;
            Material material = Material.FromMaterialCore(object3D.Material);
            List<StaticVertex> vertices = GetVertices(meshGeometry3D);
            WaveFrontModel waveFrontModel = new WaveFrontModel();
            waveFrontModel.IndexData = meshGeometry3D.Indices.ToList();
            waveFrontModel.VertexData = vertices;
            waveFrontModel.FaceCounts = new List<int> { 1 };
            waveFrontModel.MeshMaterial = new List<Material> { material };
            return waveFrontModel;
        }
         private static List<StaticVertex> GetVertices(MeshGeometry3D meshGeometry3D)
        {
            List<StaticVertex> vertices = new List<StaticVertex>();
            for(int i=0;i<meshGeometry3D.Positions.Count;i++)
            {
                vertices.Add(new StaticVertex(meshGeometry3D.Positions[i], meshGeometry3D.Normals[i],meshGeometry3D.TextureCoordinates==null?new Vector2(1,1):meshGeometry3D.TextureCoordinates[i]));
            }
            return vertices;
        }
    }

}



