﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpHelper
{
    public struct StaticVertex
    {
        public Vector3 Position { get; private set; }
        public Vector3 Normal { get; private set; }
        public Vector2 TextureCoordinate { get; private set; }
        public StaticVertex(Vector3 position,Vector3 normal,Vector2 textureCoordinate)
        {
            Position = position;
            Normal = normal;
            TextureCoordinate = textureCoordinate;
        }
        internal static bool Compare(StaticVertex a, StaticVertex b)
        {
            return a.Position == b.Position &&
                a.TextureCoordinate == b.TextureCoordinate;
        }
    }
}
