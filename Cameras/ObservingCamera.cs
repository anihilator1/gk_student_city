﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;

namespace GK_Project4
{
    class ObservingCamera : Camera
    {
        private Model model;
       public override  Matrix View
        {
            get
            {
                return Matrix.LookAtLH(position,model.Position, Vector3.UnitY);
            }
        }
        public ObservingCamera(Vector3 position, Model model) : base(position, model.Position)
        {
            this.model = model;
        }
    }
}
