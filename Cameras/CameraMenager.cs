﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GK_Project4
{
    class CameraMenager
    {
        private List<Vector3> observingCameraPositions = new List<Vector3>()
        {
            new Vector3(-6.541411f,1,-5.923573f)
        };
        public FlyingCamera FlyingCamera { get; private set; }
        private List<Camera> observingCameras;
        private List<ModelCamera> modelCameras;
        private IEnumerator<Camera> enumerator;
        public IEnumerable<ModelCamera> ModelCameras
        {
            get
            {
                foreach (ModelCamera modelCamera in modelCameras) yield return modelCamera;
            }
        }
        public CameraMenager()
        {
            FlyingCamera = new FlyingCamera(new Vector3(0, 20, 30), new Vector3(0, 0, 0));
            observingCameras = new List<Camera>();
            modelCameras = new List<ModelCamera>();
        }
        public void CreateObservingCameras(IEnumerable<Model> models)
        {
            int i = 0;
            foreach (Model model in models)
            {
                observingCameras.Add(new ObservingCamera(observingCameraPositions[i], model));
                i++;
            }

        }
        public void CreateModelCameras(IEnumerable<Model> models)
        {
            foreach (Model model in models)
            {
                modelCameras.Add(new ModelCamera(model, 1, new Vector3(model.Position.X, model.Position.Y +20, model.Position.Z + 30)));

            }
        }
        private IEnumerator<Camera> GetEnumerator()
        {
            yield return FlyingCamera;
            foreach (ObservingCamera observingCamera in observingCameras)
            {
                yield return observingCamera;
            }
            foreach (ModelCamera modelCamera in modelCameras)
            {
                yield return modelCamera;
            }
        }
        public void Update()
        {
            foreach(ModelCamera modelCamera in modelCameras)
            {
                modelCamera.Update();
            }
        }
        public Camera NextCamera()
        {
            if (enumerator == null)
            {
                enumerator = GetEnumerator();
                enumerator.MoveNext();
                return enumerator.Current;
            }
            else
            {
                if (enumerator.MoveNext())
                {
                    return enumerator.Current;
                }
                else
                {
                    enumerator = GetEnumerator();
                    enumerator.MoveNext();
                    return enumerator.Current;
                }
            }
        }
    }
}

