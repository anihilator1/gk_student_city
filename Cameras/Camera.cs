﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GK_Project4
{
    class Camera
    {
        public Vector3 position;
        public Vector3 target;
        public virtual Matrix View
        {
            get
            {
                return Matrix.LookAtLH(position, target, Vector3.UnitY);
            }
        }
        public Camera(Vector3 position, Vector3 target)
        {
            this.position = position;
            this.target = target;
        }
    }
}
