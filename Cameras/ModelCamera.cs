﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SharpDX;

namespace GK_Project4
{
    class ModelCamera : Camera
    {
        private Model model;
        private float elevation;
        private int? oldX = null;
        private int? oldY = null;
        private Vector3 oldPosition;
        public override Matrix View
        {
            get
            {
                return Matrix.LookAtLH(position, target, Vector3.UnitY);
            }
        }
        public void Update()
        {
            Vector3 newPosition = new Vector3(model.Position.X, model.Position.Y + elevation, model.Position.Z);
            Vector3 translation = target - oldPosition;
            Vector3 newTarget = newPosition + translation;
            target = newTarget;
            position = newPosition;
            oldPosition = newPosition;
        }
        public ModelCamera(Model model, float elevation, Vector3 target) : base(new Vector3(model.Position.X, model.Position.Y + elevation, model.Position.Z), target)
        {
            this.model = model;
            this.oldPosition = new Vector3(model.Position.X, model.Position.Y + elevation, model.Position.Z);
            this.elevation = elevation;
        }
        public void UpdateAngle(object sender, MouseEventArgs mouseEventArgs)
        {
            if (mouseEventArgs.Button == MouseButtons.Left)
            {
                if (mouseEventArgs.X - oldX > 0)
                {
                    Vector3 tmp = target - position;
                    tmp.Y = 0;
                    Matrix matrix = Matrix.RotationY((float)(mouseEventArgs.X - oldX) / 100f);
                    tmp = Vector3.TransformCoordinate(tmp, matrix);
                    target.X = tmp.X + position.X;
                    target.Z = tmp.Z + position.Z;
                }
                if (mouseEventArgs.X - oldX < 0)
                {
                    Vector3 tmp = target - position;
                    tmp.Y = 0;
                    Matrix matrix = Matrix.RotationY((float)(mouseEventArgs.X - oldX) / 100f);
                    tmp = Vector3.TransformCoordinate(tmp, matrix);
                    target.X = tmp.X + position.X;
                    target.Z = tmp.Z + position.Z;
                }
                if (mouseEventArgs.Y - oldY > 0)
                {
                    target.Y -= (float)(mouseEventArgs.Y - oldY) / 10f;
                }
                if (mouseEventArgs.Y - oldY < 0)
                {
                    target.Y -= (float)(mouseEventArgs.Y - oldY) / 10f;
                }


            }
            oldX = mouseEventArgs.X;
            oldY = mouseEventArgs.Y;
        }

    }
}
