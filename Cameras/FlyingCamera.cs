﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GK_Project4
{
    class FlyingCamera :Camera
    {
        private int? oldX = null;
        private int? oldY = null;
        public bool Enabled { get; set; }
        public FlyingCamera(Vector3 position, Vector3 target): base(position,target){}
        public void UpdatePosition(object sender, KeyEventArgs keyEventArgs)
        {
            if (!Enabled) return;
            Vector3 direction = target - position;
            direction.Normalize();
            Vector3 perpendicular = new Vector3(-direction.Z, 0, direction.X);
            perpendicular.Normalize();
            switch (keyEventArgs.KeyCode)
            {
                case Keys.W:
                    target += direction;
                    position += direction;
                    break;
                case Keys.S:
                    target -= direction;
                    position -= direction;
                    break;
                case Keys.A:
                    target += perpendicular;
                    position += perpendicular;
                    break;
                case Keys.D:
                    target -= perpendicular;
                    position -= perpendicular;
                    break;

            }
            Enabled = false;
        }
        public void UpdateAngle(object sender, MouseEventArgs mouseEventArgs)
        {
            if (mouseEventArgs.Button == MouseButtons.Left)
            {
                if (mouseEventArgs.X - oldX > 0)
                {
                    Vector3 tmp = target - position;
                    tmp.Y = 0;
                    Matrix matrix = Matrix.RotationY((float)(mouseEventArgs.X - oldX) / 100f);
                    tmp = Vector3.TransformCoordinate(tmp, matrix);
                    target.X = tmp.X + position.X;
                    target.Z = tmp.Z + position.Z;
                }
                if (mouseEventArgs.X - oldX < 0)
                {
                    Vector3 tmp = target - position;
                    tmp.Y = 0;
                    Matrix matrix = Matrix.RotationY((float)(mouseEventArgs.X - oldX) / 100f);
                    tmp = Vector3.TransformCoordinate(tmp, matrix);
                    target.X = tmp.X + position.X;
                    target.Z = tmp.Z + position.Z;
                }
                if (mouseEventArgs.Y - oldY > 0)
                {
                    target.Y -= (float)(mouseEventArgs.Y - oldY) / 10f;
                }
                if (mouseEventArgs.Y - oldY < 0)
                {
                    target.Y -= (float)(mouseEventArgs.Y - oldY) / 10f;
                }


            }
            oldX = mouseEventArgs.X;
            oldY = mouseEventArgs.Y;
        }

    }
}
