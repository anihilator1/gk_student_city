﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GK_Project4
{
    class FPSLimiter
    {
        private Stopwatch stopwatch;
        private int FPSLimit;
        public FPSLimiter(int FPSLimit)
        {
            this.stopwatch = new Stopwatch();
            this.FPSLimit = FPSLimit;
            stopwatch.Start();
        }
        public void LimitFPS()
        {
            long delta = stopwatch.ElapsedMilliseconds;
            if (delta < 1000.0 / FPSLimit)
            {
                int sleepTime = (int)((1000.0 / FPSLimit) - delta);
                System.Threading.Thread.Sleep(sleepTime);
            }
            stopwatch.Restart();
        }
    }
}
