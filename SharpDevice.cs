﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Windows;

using Device11 = SharpDX.Direct3D11.Device;
using Buffer11 = SharpDX.Direct3D11.Buffer;

namespace SharpHelper
{
    public class SharpDevice : IDisposable
    {
        private Device11 _device;
        private DeviceContext _deviceContext;
        private SwapChain _swapchain;
        private RenderTargetView _backbufferView;
        private DepthStencilView _zbufferView;
        private RasterizerState _rasterState;
        private BlendState _blendState;
        private DepthStencilState _depthState;
        private SamplerState _samplerState;
        public Device11 Device { get { return _device; } }
        public DeviceContext DeviceContext { get { return _deviceContext; } }
        public SwapChain SwapChain { get { return _swapchain; } }
        public RenderForm View { get; private set; }
        public bool MustResize { get; private set; }
        public RenderTargetView BackBufferView { get { return _backbufferView; } }
        public DepthStencilView ZBufferView { get { return _zbufferView; } }
        public Sharp2D Font { get; private set; }
        public SharpDevice(RenderForm form, bool debug = false)
        {
            View = form;
            var desc = new SwapChainDescription()
            {
                BufferCount = 1,
                ModeDescription = new ModeDescription(View.ClientSize.Width, View.ClientSize.Height, new Rational(60, 1), Format.R8G8B8A8_UNorm),//sview
                IsWindowed = true,
                OutputHandle = View.Handle,
                SampleDescription = new SampleDescription(1, 0),
                SwapEffect = SwapEffect.Discard,
                Usage = Usage.RenderTargetOutput
            };

            FeatureLevel[] levels = new FeatureLevel[] { FeatureLevel.Level_11_1 };

            DeviceCreationFlags flag = DeviceCreationFlags.None | DeviceCreationFlags.BgraSupport;
            if (debug)
                flag = DeviceCreationFlags.Debug;

            Device11.CreateWithSwapChain(SharpDX.Direct3D.DriverType.Hardware, flag, levels, desc, out _device, out _swapchain);

            _deviceContext = Device.ImmediateContext;

            var factory = SwapChain.GetParent<Factory>();
            factory.MakeWindowAssociation(View.Handle, WindowAssociationFlags.IgnoreAll);

            View.UserResized += (sender, args) => MustResize = true;

            SetDefaultRasterState();
            SetDefaultDepthState();
            SetDefaultBlendState();
            SetDefaultSamplerState();

            Font = new Sharp2D(this);

            Resize();
        }
        public void Resize()
        {
            Font.Release();
            Utilities.Dispose(ref _backbufferView);
            Utilities.Dispose(ref _zbufferView);
            
            if (View.ClientSize.Width == 0 || View.ClientSize.Height == 0)
                return;

            SwapChain.ResizeBuffers(1, View.ClientSize.Width, View.ClientSize.Height, Format.R8G8B8A8_UNorm, SwapChainFlags.None);

            var _backBufferTexture = SwapChain.GetBackBuffer<Texture2D>(0);

            Font.UpdateResources(_backBufferTexture);

            _backbufferView = new RenderTargetView(Device, _backBufferTexture);
            _backBufferTexture.Dispose();

            var _zbufferTexture = new Texture2D(Device, new Texture2DDescription()
            {
                Format = Format.D16_UNorm,
                ArraySize = 1,
                MipLevels = 1,
                Width = View.ClientSize.Width,
                Height = View.ClientSize.Height,
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.DepthStencil,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None
            });

            _zbufferView = new DepthStencilView(Device, _zbufferTexture);
            _zbufferTexture.Dispose();

            SetDefaultTargers();
            
            MustResize = false;
        }
        public void SetDefaultTargers()
        {
            DeviceContext.Rasterizer.SetViewport(0, 0, View.ClientSize.Width, View.ClientSize.Height);
            DeviceContext.OutputMerger.SetTargets(_zbufferView, _backbufferView);
        }
        public void Dispose()
        {
            Font.Dispose();
            _rasterState.Dispose();
            _blendState.Dispose();
            _depthState.Dispose();
            _samplerState.Dispose();

            _backbufferView.Dispose();
            _zbufferView.Dispose();
            _swapchain.Dispose();
            _deviceContext.Dispose();
            _device.Dispose();
        }
        public void Clear(Color4 color)
        {
            DeviceContext.ClearRenderTargetView(_backbufferView, color);
            DeviceContext.ClearDepthStencilView(_zbufferView, DepthStencilClearFlags.Depth, 1.0F, 0);
        }
        public void Present()
        {
            SwapChain.Present(0, PresentFlags.None);
        }
        public void SetDefaultRasterState()
        {
            Utilities.Dispose(ref _rasterState);
            RasterizerStateDescription rasterDescription = RasterizerStateDescription.Default();
            _rasterState = new RasterizerState(Device, rasterDescription);
            DeviceContext.Rasterizer.State = _rasterState;
        }
        public void SetWireframeRasterState()
        {
            _rasterState.Dispose();
            RasterizerStateDescription rasterDescription = RasterizerStateDescription.Default();
            rasterDescription.FillMode = FillMode.Wireframe;
            _rasterState = new RasterizerState(Device, rasterDescription);

            DeviceContext.Rasterizer.State = _rasterState;
        }
        public void SetDefaultBlendState()
        {
            Utilities.Dispose(ref _blendState);
            BlendStateDescription description = BlendStateDescription.Default();
            _blendState = new BlendState(Device, description);
        }
        public void SetBlend(BlendOperation operation, BlendOption source, BlendOption destination)
        {
            Utilities.Dispose(ref _blendState);
            BlendStateDescription description = BlendStateDescription.Default();

            description.RenderTarget[0].BlendOperation = operation;
            description.RenderTarget[0].SourceBlend = source;
            description.RenderTarget[0].DestinationBlend = destination;
            description.RenderTarget[0].IsBlendEnabled = true;
            _blendState = new BlendState(Device, description);
        }
        public void SetDefaultDepthState()
        {
            Utilities.Dispose(ref _depthState);
            DepthStencilStateDescription description = DepthStencilStateDescription.Default();
            description.DepthComparison = Comparison.LessEqual;
            description.IsDepthEnabled = true;

            _depthState = new DepthStencilState(Device, description);
        }
        public void SetDefaultSamplerState()
        {
            Utilities.Dispose(ref _samplerState);
            SamplerStateDescription description = SamplerStateDescription.Default();
            description.Filter = Filter.MinMagMipLinear;
            description.AddressU = TextureAddressMode.Wrap;
            description.AddressV = TextureAddressMode.Wrap;
            _samplerState = new SamplerState(Device, description);
        }
        public void UpdateAllStates()
        {
            DeviceContext.Rasterizer.State = _rasterState;
            DeviceContext.OutputMerger.SetBlendState(_blendState);
            DeviceContext.OutputMerger.SetDepthStencilState(_depthState);
            DeviceContext.PixelShader.SetSampler(0, _samplerState);
        }
        public void UpdateData<T>(Buffer11 buffer, T data) where T : struct
        {
            DeviceContext.UpdateSubresource(ref data, buffer);
        }
        public void ApplyMultipleRenderTarget(params SharpRenderTarget[] targets)
        {
            var targetsView = targets.Select(t => t.Target).ToArray();
            DeviceContext.OutputMerger.SetTargets(targets[0].Zbuffer, targetsView);
            DeviceContext.Rasterizer.SetViewport(0, 0, targets[0].Width, targets[0].Height);
        }
        public static bool IsDirectX11Supported()
        {
            return SharpDX.Direct3D11.Device.GetSupportedFeatureLevel() == FeatureLevel.Level_11_0;
        }
    }
}
