﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Buffer11 = SharpDX.Direct3D11.Buffer;

namespace SharpHelper
{
    public class SharpRenderTarget : IDisposable
    {
        public SharpDevice Device { get; private set; }
        public RenderTargetView Target
        {
            get { return _target; }
        }
        public DepthStencilView Zbuffer
        {
            get { return _zbuffer; }
        }
        public ShaderResourceView Resource
        {
            get { return _resource; }
        }
        public int Width { get;  set; }
        public int Height { get;  set; }
        private RenderTargetView _target;
        private DepthStencilView _zbuffer;
        private ShaderResourceView _resource;
        public SharpRenderTarget(SharpDevice device, int width, int height, Format format)
        {
            Device = device;
            Height = height;
            Width = width;

            Texture2D target = new Texture2D(device.Device, new Texture2DDescription()
            {
                Format = format,
                Width = width,
                Height = height,
                ArraySize = 1,
                BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                CpuAccessFlags = CpuAccessFlags.None,
                MipLevels = 1,
                OptionFlags = ResourceOptionFlags.None,
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
            });

            _target = new RenderTargetView(device.Device, target);
            _resource = new ShaderResourceView(device.Device, target);
             target.Dispose();

            var _zbufferTexture = new Texture2D(Device.Device, new Texture2DDescription()
            {
                Format = Format.D16_UNorm,
                ArraySize = 1,
                MipLevels = 1,
                Width = width,
                Height = height,
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.DepthStencil,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None
            });

            // Create the depth buffer view
            _zbuffer = new DepthStencilView(Device.Device, _zbufferTexture);
            _zbufferTexture.Dispose();

        }
        public void Apply()
        {
            Device.DeviceContext.Rasterizer.SetViewport(0, 0, Width, Height);
            Device.DeviceContext.OutputMerger.SetTargets(_zbuffer, _target);
        }
        public void Dispose()
        {
            _resource.Dispose();
            _target.Dispose();
            _zbuffer.Dispose();
        }
        public void Clear(Color4 color)
        {
            Device.DeviceContext.ClearRenderTargetView(_target, color);
            Device.DeviceContext.ClearDepthStencilView(_zbuffer, DepthStencilClearFlags.Depth, 1.0F, 0);
        }
    }
}
