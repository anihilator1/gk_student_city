﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GK_Project4;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Windows;
using SharpHelper;
using SharpDxBuffer = SharpDX.Direct3D11.Buffer;


namespace GK_Project4
{
    class ShaderMenager
    {
        private SharpDevice sharpDevice;
        private SharpShader sharpShader;
        private SharpDxBuffer sharpDxBuffer;
        private SharpShader phongShader;
        private SharpShader gouraudShader;
        private SharpDxBuffer phongSharpDxBuffer;
        private SharpDxBuffer gouraudSharpDxBuffer;
        private ShaderData shaderData;
        public ShaderMenager(SharpDevice sharpDevice)
        {
            this.gouraudShader= GetShader(sharpDevice, "../../Shader/GouraudShader.hlsl");
            this.gouraudSharpDxBuffer= gouraudShader.CreateBuffer<ShaderData>();

            this.phongShader = GetShader(sharpDevice, "../../Shader/PhongShader.hlsl");
            this.phongSharpDxBuffer = phongShader.CreateBuffer<ShaderData>();

            sharpShader = phongShader;
            sharpDxBuffer = phongSharpDxBuffer;

            this.shaderData = GetDefaultShaderData();
            this.sharpDevice = sharpDevice;
        }
        private SharpShader GetShader(SharpDevice sharpDevice, string path)
        {
            SharpShaderDescription sharpShaderDescription = new SharpShaderDescription();
            sharpShaderDescription.VertexShaderFunction = "VS";
            sharpShaderDescription.PixelShaderFunction = "PS";
            SharpShader sharpShader = new SharpShader(sharpDevice, path, sharpShaderDescription,
                  new InputElement[] {
                        new InputElement("POSITION", 0, Format.R32G32B32_Float, 0, 0),
                        new InputElement("NORMAL", 0, Format.R32G32B32_Float, 12, 0),
                        new InputElement("TEXCOORD", 0, Format.R32G32_Float, 24, 0),
                  });
            return sharpShader;
        }
        public void ApplyShader()
        {
            sharpShader.Apply();
        }
        private ShaderData GetDefaultShaderData()
        {
            ShaderData shaderData = new ShaderData()
            {
                fogOn = 1,
                fogColor = 0.5f,
                min = 0,
                max = 25,
                pointLightPosition1 = new Vector4(-4.544548f, 2.670131f, -47.24805f, 1),
                pointLightPosition2 = new Vector4(0.9554499f, 2.670131f, -47.24805f, 1),
                pointLightPosition3 = new Vector4(6.755446f, 2.670131f, -47.24805f, 1),
                pointLightPosition4 = new Vector4(12.75546f, 2.670131f, -47.24805f, 1),
                pointLightPosition5 = new Vector4(17.45548f, 2.670131f, -47.24805f, 1),

                pointLightPosition6 = new Vector4(25.55551f, 2.670131f, -46.14807f, 1),
                pointLightPosition7 = new Vector4(31.95554f, 2.670131f, -43.7481f, 1),
                pointLightPosition8 = new Vector4(37.75545f, 2.670131f, -38.94818f, 1),
                pointLightPosition9 = new Vector4(42.75537f, 2.670131f, -30.64828f, 1),
                pointLightPosition10 = new Vector4(43.75536f, 2.670131f, -22.94825f, 1),

                pointLightPosition11 = new Vector4(50.65525f, 2.670131f, 13.75182f, 1),
                pointLightPosition12 = new Vector4(55.25518f, 2.670131f, 16.95183f, 1),
                pointLightPosition13 = new Vector4(59.95511f, 2.670131f, 20.25184f, 1),
                pointLightPosition14 = new Vector4(64.85504f, 2.670131f, 23.65186f, 1),
                pointLightPosition15 = new Vector4(68.75498f, 2.670131f, 26.35187f, 1),

                pointLightPosition16 = new Vector4(78.75483f, 2.670131f, 37.15181f, 1),
                pointLightPosition17 = new Vector4(80.6548f, 2.670131f, 44.4517f, 1),
                pointLightPosition18 = new Vector4(80.15481f, 2.670131f, 54.15155f, 1),
                pointLightPosition19 = new Vector4(76.55486f, 2.670131f, 61.15144f, 1),
                pointLightPosition20 = new Vector4(74.85489f, 2.670131f, 31.85189f, 1),

                pointLightPosition21 = new Vector4(70.65495f, 2.670131f, 68.25134f, 1),
                pointLightPosition22 = new Vector4(65.05504f, 2.670131f, 72.15128f, 1),
                pointLightPosition23 = new Vector4(57.85515f, 2.670131f, 74.15125f, 1),
                pointLightPosition24 = new Vector4(48.1553f, 2.670131f, 73.65125f, 1),
                pointLightPosition25 = new Vector4(41.1554f, 2.670131f, 70.05131f, 1),

                pointLightPosition26 = new Vector4(33.25552f, 2.670131f, 64.85139f, 1),
                pointLightPosition27 = new Vector4(28.65553f, 2.670131f, 61.75143f, 1),
                pointLightPosition28 = new Vector4(24.05551f, 2.670131f, 58.65148f, 1),
                pointLightPosition29 = new Vector4(18.95549f, 2.670131f, 55.05154f, 1),
                pointLightPosition30 = new Vector4(15.05547f, 2.670131f, 52.45158f, 1),

                pointLightPosition31 = new Vector4(1.955452f, 2.670131f, 47.85165f, 1),
                pointLightPosition32 = new Vector4(-5.144546f, 2.670131f, 45.95168f, 1),
                pointLightPosition33 = new Vector4(-12.54456f, 2.670131f, 44.0517f, 1),
                pointLightPosition34 = new Vector4(-20.24459f, 2.670131f, 41.95174f, 1),
                pointLightPosition35 = new Vector4(-26.24461f, 2.670131f, 40.55176f, 1),

                pointLightPosition36 = new Vector4(-36.64456f, 2.670131f, 38.65179f, 1),
                pointLightPosition37 = new Vector4(-42.44447f, 2.670131f, 36.55182f, 1),
                pointLightPosition38 = new Vector4(-48.34438f, 2.670131f, 31.85189f, 1),
                pointLightPosition39 = new Vector4(-53.5443f, 2.670131f, 23.65186f, 1),
                pointLightPosition40 = new Vector4(-54.64429f, 2.670131f, 15.95183f, 1),

                pointLightPosition41 = new Vector4(-54.94428f, 2.670131f, 5.851798f, 1),
                pointLightPosition42 = new Vector4(-54.94428f, 2.670131f, 0.3518013f, 1),
                pointLightPosition43 = new Vector4(-54.94428f, 2.670131f, -5.448196f, 1),
                pointLightPosition44 = new Vector4(-54.94428f, 2.670131f, -11.54821f, 1),
                pointLightPosition45 = new Vector4(-54.94428f, 2.670131f, -16.14822f, 1),

                pointLightPosition46 = new Vector4(-54.64429f, 2.670131f, -25.24826f, 1),
                pointLightPosition47 = new Vector4(-52.14433f, 2.670131f, -34.34825f, 1),
                pointLightPosition48 = new Vector4(-48.34438f, 2.670131f, -41.04815f, 1),
                pointLightPosition49 = new Vector4(-44.14445f, 2.670131f, -46.34806f, 1),
                pointLightPosition50 = new Vector4(-35.24458f, 2.670131f, -47.24805f, 1),

                pointLightPosition51 = new Vector4(-29.64462f, 2.670131f, -47.24805f, 1),
                pointLightPosition52 = new Vector4(-23.8446f, 2.670131f, -47.24805f, 1),
                pointLightPosition53 = new Vector4(-17.84458f, 2.670131f, -47.24805f, 1),
                pointLightPosition54 = new Vector4(-13.14456f, 2.670131f, -47.24805f, 1),

                turnOnReflectors = 1,
                turnOnLights = 1,
                trunOnCameraLight = 0,
                turnOnTexture = 1,

                ambientLight = new Vector4(0.6f, 0.6f, 0.6f, 1),
               
            };
            return shaderData;
        }
        public void Update(Matrix world, Matrix projection, Camera camera, IEnumerator<RoadModel> enumerator,FlyingCamera flyingCamera)
        {
            float elevation=1;
            shaderData.world = world;
            shaderData.worldViewProjection = world * camera.View * projection;
            shaderData.lightDirection = new Vector4(camera.position - camera.target, 1);
            shaderData.cameraPosition = new Vector4(camera.position, 1);


            enumerator.MoveNext();
            shaderData.reflectorPostion1 = new Vector4(enumerator.Current.Position.X,elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection1 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion2 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection2 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion3 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection3 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion4 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection4 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion5 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection5 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion6 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection6 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion7 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection7 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion8 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection8 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion9 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection9 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion10 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection10 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion11 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection11 = enumerator.Current.Direction;

            enumerator.MoveNext();
            shaderData.reflectorPostion12 = new Vector4(enumerator.Current.Position.X, elevation, enumerator.Current.Position.Z, 1);
            shaderData.reflectorDirection12 = enumerator.Current.Direction;

            shaderData.reflectorPostion13= new Vector4(flyingCamera.position, 1);
            shaderData.reflectorDirection13 = new Vector4(flyingCamera.target-flyingCamera.position, 1);
            

            sharpDevice.UpdateData<ShaderData>(sharpDxBuffer, shaderData);
            sharpDevice.DeviceContext.VertexShader.SetConstantBuffer(0, sharpDxBuffer);
            sharpDevice.DeviceContext.PixelShader.SetConstantBuffer(0, sharpDxBuffer);
        }
        public void Dispose()
        {
            sharpDxBuffer.Dispose();
            sharpShader.Dispose();
        }
        public void FogControl(object sender, KeyEventArgs keyEventArgs)
        {
            switch (keyEventArgs.KeyCode)
            {
                case Keys.F:
                    shaderData.fogOn = shaderData.fogOn == 0 ? 1 : 0;
                    break;
                case Keys.D1:
                    if (shaderData.fogColor - 0.1f >= 0) shaderData.fogColor -= 0.01f;
                    break;
                case Keys.D2:
                    if (shaderData.fogColor + 0.1f <= 1) shaderData.fogColor += 0.01f;
                    break;
                case Keys.D3:
                    if (shaderData.max - 0.1f > shaderData.min) shaderData.max -= 0.1f;
                    break;
                case Keys.D4:
                    shaderData.max += 0.1f;
                    break;
                case Keys.D5:
                    if (shaderData.min - 0.1f > 0) shaderData.min -= 0.1f;
                    break;
                case Keys.D6:
                    if (shaderData.min + 0.1f < shaderData.max) shaderData.min += 0.1f;
                    break;
            }

        }
        public void LightControl(object sender, KeyEventArgs keyEventArgs)
        {
            switch (keyEventArgs.KeyCode)
            {
                case Keys.I:
                    shaderData.turnOnLights = shaderData.turnOnLights == 0 ? 1 : 0;
                    break;
                case Keys.O:
                    shaderData.turnOnReflectors = shaderData.turnOnReflectors == 0 ? 1 : 0;
                    break;
                case Keys.P:
                    shaderData.trunOnCameraLight = shaderData.trunOnCameraLight == 0 ? 1 : 0;
                    break;
                case Keys.N:
                    shaderData.ambientLight = shaderData.ambientLight ==new Vector4(0.6f, 0.6f, 0.6f, 1)? new Vector4(0.05f, 0.05f, 0.05f, 1) : new Vector4(0.6f, 0.6f, 0.6f, 1);
                    shaderData.fogColor = shaderData.fogColor == 0.5f ? 0 : 0.5f;
                    break;
                case Keys.G:
                    sharpShader = sharpShader == phongShader ? gouraudShader : phongShader;
                    sharpDxBuffer = sharpDxBuffer == phongSharpDxBuffer ? gouraudSharpDxBuffer : phongSharpDxBuffer;
                    break;
                case Keys.T:
                    shaderData.turnOnTexture = shaderData.turnOnTexture == 0 ? 1 : 0;
                    break;
            }

        }
        public void AddEffectsControl(RenderForm renderForm)
        {
            renderForm.KeyDown += FogControl;
            renderForm.KeyDown += LightControl;
        }
    }
}
