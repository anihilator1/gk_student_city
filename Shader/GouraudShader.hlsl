
cbuffer data :register(b0)
{
	float4x4 world;
	float4x4 worldViewProj;
	float4 lightDirection;
	float4 cameraPosition;
	int fogOn;
	float fogColor;
	float min;
	float max;
	float4 lightPositions[54];
	float4 reflectorPositions[13];
	float4 reflectorDirections[13];
	int turnOnLights;
	int turnOnReflectors;
	int trunOnCameraLight;
	int turnOnTexture;
	float4 ambientLight;
};

struct VS_IN
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD;
};

struct PS_IN
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD;
	float4 transformedPosition :POSITIONT;
	float4 lightSum: LIGHT;
};

//texture
Texture2D textureMap;
SamplerState textureSampler;



float4 addFog(float4 color, float4 pos)
{
	if (fogOn == 0) return color;
	float distanceFromCamera = distance(cameraPosition, pos);
	float fog;
	if (distanceFromCamera > max) fog = 1;
	else if (distanceFromCamera < min) fog = 0;
	else fog = 1 - (max - distanceFromCamera) / (max - min);
	float4 resultColor = (1 - fog)*color + fog * fogColor;
	return resultColor;
}

float4 phongFormula(float4 LColor, float3 N, float3 L, float3 V, float3 R)
{
	float4 Ia = 0.1 * ambientLight;
	float4 Id = 0.5 * saturate(dot(N, L));
	float4 Is = 0.5 * pow(saturate(dot(R, V)), 30);

	return  Ia + (Id + Is) * LColor;
}
float computeDecline(float4 lightPosition, float4 position)
{
	float dst = distance(lightPosition, position);
	float constant = 1.0f;
	float lin = 0.014f;
	float quadratic = 0.07f;
	float decline = 1.0 / (constant + lin * dst + quadratic * (dst * dst));
	return decline;
}
float computeReflectorDecline(float4 lightPosition, float4 position)
{
	float dst = distance(lightPosition, position);
	float constant = 1.0f;
	float lin = 0.014f;
	float quadratic = 0.0007f;
	float decline = 1.0 / (constant + lin * dst + quadratic * (dst * dst));
	return decline;
}
float4 reflectorLight(PS_IN input, float4 position, float4 direction)
{
	//if (pos.w == 0)
	//	return float4(0, 0, 0, 0);

	float3 lightDir = normalize(input.transformedPosition.xyz - position.xyz);
	float theta = dot(lightDir.xyz, normalize(direction.xyz));

	input.normal = normalize(input.normal);
	float3 V = normalize(cameraPosition.xyz - (float3) input.transformedPosition);
	float3 R = reflect(lightDir.xyz, input.normal);
	float decline = computeReflectorDecline(position, input.transformedPosition);

	if (theta > 0) return decline*pow(theta, 50) * phongFormula(float4(1, 1, 0.5, 1), input.normal, -lightDir, V, R);
	else return float4(0, 0, 0, 0);
}
float4 computeLight(PS_IN input)
{
	input.normal = normalize(input.normal);

	float3 V = normalize(cameraPosition.xyz - input.transformedPosition.xyz);
	float4 result = ambientLight;
	if (turnOnLights == 1)
	{
		for (int i = 0; i < 54; i++)
		{
			if (lightPositions[i].w == 0) continue;

			float4 lightDir = normalize(input.transformedPosition - lightPositions[i]);
			float3 R = reflect(lightDir.xyz, input.normal);
			float decline = computeDecline(lightPositions[i], input.transformedPosition);
			result += decline * phongFormula(float4(1, 1, 0.7, 1), input.normal, -lightDir.xyz, V, R);
		}
	}

	for (int i = 0; i < 13; i++)
	{
		if (reflectorPositions[i].w == 0) continue;
		if (trunOnCameraLight == 0 && i == 12) continue;
		if (turnOnReflectors == 0 && i < 12) continue;

		float3 lightDir = normalize(input.transformedPosition.xyz - reflectorPositions[i].xyz);
		float alfa = dot(lightDir.xyz, normalize(reflectorDirections[i]));

		input.normal = normalize(input.normal);
		float3 V = normalize(cameraPosition.xyz - (float3) input.transformedPosition);
		float3 R = reflect(lightDir.xyz, input.normal);

		float decline = computeReflectorDecline(reflectorPositions[i], input.transformedPosition);

		if (alfa > 0) result += decline * pow(alfa, 25) * phongFormula(float4(1, 1, 0.7, 1), input.normal, -lightDir, V, R);
	}


	if (result.x > 1) result.x = 1;
	if (result.y > 1) result.y = 1;
	if (result.z > 1) result.z = 1;

	return result;
}
PS_IN VS(VS_IN input)
{
	PS_IN output = (PS_IN)0;

	output.position = mul(worldViewProj, input.position);
	output.normal = normalize(mul(world, input.normal));
	output.texcoord = input.texcoord;
	output.transformedPosition = mul(world, input.position);

	output.lightSum = computeLight(output);
	return output;
}
float4 PS(PS_IN input) : SV_Target
{	
	float4 result = input.lightSum;
	if (turnOnTexture == 1)
	{
		result *= textureMap.Sample(textureSampler, input.texcoord);
	}
	return  addFog(result,input.transformedPosition);
}