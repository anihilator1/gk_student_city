﻿using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpHelper
{
    public class SharpShaderDescription
    {
        public string VertexShaderFunction { get; set; }
        public string PixelShaderFunction { get; set; }
        public string GeometryShaderFunction { get; set; }
        public string HullShaderFunction { get; set; }
        public string DomainShaderFunction { get; set; }
        public StreamOutputElement[] GeometrySO { get; set; }
    }
}
