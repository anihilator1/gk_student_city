﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GK_Project4
{
    struct ShaderData
    {
        public Matrix world;
        public Matrix worldViewProjection;
        public Vector4 lightDirection;

        //fog
        public Vector4 cameraPosition;
        public int fogOn;
        public float fogColor;
        public float min;
        public float max;

        //Point lights
        public Vector4 pointLightPosition1;
        public Vector4 pointLightPosition2;
        public Vector4 pointLightPosition3;
        public Vector4 pointLightPosition4;
        public Vector4 pointLightPosition5;

        public Vector4 pointLightPosition6;
        public Vector4 pointLightPosition7;
        public Vector4 pointLightPosition8;
        public Vector4 pointLightPosition9;
        public Vector4 pointLightPosition10;

        public Vector4 pointLightPosition11;
        public Vector4 pointLightPosition12;
        public Vector4 pointLightPosition13;
        public Vector4 pointLightPosition14;
        public Vector4 pointLightPosition15;

        public Vector4 pointLightPosition16;
        public Vector4 pointLightPosition17;
        public Vector4 pointLightPosition18;
        public Vector4 pointLightPosition19;
        public Vector4 pointLightPosition20;

        public Vector4 pointLightPosition21;
        public Vector4 pointLightPosition22;
        public Vector4 pointLightPosition23;
        public Vector4 pointLightPosition24;
        public Vector4 pointLightPosition25;

        public Vector4 pointLightPosition26;
        public Vector4 pointLightPosition27;
        public Vector4 pointLightPosition28;
        public Vector4 pointLightPosition29;
        public Vector4 pointLightPosition30;

        public Vector4 pointLightPosition31;
        public Vector4 pointLightPosition32;
        public Vector4 pointLightPosition33;
        public Vector4 pointLightPosition34;
        public Vector4 pointLightPosition35;

        public Vector4 pointLightPosition36;
        public Vector4 pointLightPosition37;
        public Vector4 pointLightPosition38;
        public Vector4 pointLightPosition39;
        public Vector4 pointLightPosition40;

        public Vector4 pointLightPosition41;
        public Vector4 pointLightPosition42;
        public Vector4 pointLightPosition43;
        public Vector4 pointLightPosition44;
        public Vector4 pointLightPosition45;

        public Vector4 pointLightPosition46;
        public Vector4 pointLightPosition47;
        public Vector4 pointLightPosition48;
        public Vector4 pointLightPosition49;
        public Vector4 pointLightPosition50;

        public Vector4 pointLightPosition51;
        public Vector4 pointLightPosition52;
        public Vector4 pointLightPosition53;
        public Vector4 pointLightPosition54;

        public Vector4 reflectorPostion1;
        public Vector4 reflectorPostion2;
        public Vector4 reflectorPostion3;
        public Vector4 reflectorPostion4;
        public Vector4 reflectorPostion5;
        public Vector4 reflectorPostion6;
        public Vector4 reflectorPostion7;
        public Vector4 reflectorPostion8;
        public Vector4 reflectorPostion9;
        public Vector4 reflectorPostion10;
        public Vector4 reflectorPostion11;
        public Vector4 reflectorPostion12;
        public Vector4 reflectorPostion13;


        public Vector4 reflectorDirection1;
        public Vector4 reflectorDirection2;
        public Vector4 reflectorDirection3;
        public Vector4 reflectorDirection4;
        public Vector4 reflectorDirection5;
        public Vector4 reflectorDirection6;
        public Vector4 reflectorDirection7;
        public Vector4 reflectorDirection8;
        public Vector4 reflectorDirection9;
        public Vector4 reflectorDirection10;
        public Vector4 reflectorDirection11;
        public Vector4 reflectorDirection12;
        public Vector4 reflectorDirection13;

        public int turnOnLights;
        public int turnOnReflectors;
        public int trunOnCameraLight;
        public int turnOnTexture;

        public Vector4 ambientLight;
    }
}
